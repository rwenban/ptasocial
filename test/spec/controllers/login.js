'use strict';

describe('Controller: loginCtrl', function () {

  // load the controller's module
  beforeEach(module('ptasocialApp'));

  var LoginCtrl,
    scope,
    httpBackend,
    http;

  // var _isCollapsed = false;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (emailvalidation, $controller, $rootScope, $httpBackend, $http) {
    scope = $rootScope.$new();
    LoginCtrl = $controller('loginCtrl', {
      $scope: scope
    });

    httpBackend = $httpBackend;

    http = $http;

    // TODO
    // spyOn( scope.isCollapsed, scope.login );
  }));

  it('simple test for scope defined var', function () {
    expect(scope.val).toBeDefined();
  });

  it('should have a LoginCtrl controller', function () {
    expect(LoginCtrl).toBeDefined();
  });

  it('should have a property isCollapsed', function () {
    expect(scope.isCollapsed).toBeDefined();
  });

  it('should have a value of false', function () {
    expect(scope.isCollapsed).toBeFalsy();
  });

  it('should have a session property', function () {
    expect(scope.session).toBeDefined();
  });

  it('should have a login function "by accessor"', function () {
    expect(scope.login).toBeDefined();
  });

});