'use strict';

describe('Controller: SampleWizardCtrl', function () {

  // load the controller's module
  beforeEach(module('ptasocialApp'));

  var SampleWizardCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SampleWizardCtrl = $controller('SampleWizardCtrl', {
      $scope: scope
    });
  }));

  it('simple test for scope defined var', function () {
    expect(scope.val).toBeDefined();
  });

});
