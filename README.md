# PTA Social README

## This document is for technical issues relating to the project.

### Environment

I've developed the project on Mac OSX 10.9, using [Webstorm 8](http://www.jetbrains.com/webstorm/whatsnew/).

### Source Control

I've used Git.

Currently I've been using Bitbucket to maintain the code. The project can be found on: [ptasocial](https://bitbucket.org/rwenban/ptasocial).

For SSH setup see: [Set up SSH for Git and Mercurial on Mac OSX/Linux](https://confluence.atlassian.com/pages/viewpage.action;jsessionid=9038BB82AB06F0DB1A27D20618B4845B?pageId=270827678).

### Angular / Yeoman Setup

The project has been created by [Yeoman](http://yeoman.io).

There are a number of global dependencies required to 'run' the app which include:

* Homebrew
* Node ( NPM )
* Bower
* Jasmine
* Grunt

Install globals

    $ npm install -g grunt

    $ npm install -g bower

    $ npm install -g grunt-cli


    $ brew doctor

    $ brew update



I'll attempt to detail them in due course.

### Setup

Clone the project,

    $ git clone git@bitbucket.org:rwenban/ptasocial.git

Local dependencies:

    $ npm install


### Typical errors and some solutions


    Fatal error: Unable to find local grunt.



    # to install locally
    $ npm install grunt --save-dev



    # to update local dependencies
    $ bower update


### Iterative development

The sequence is modify the code then use Grunt:


    # Will run tests and minify
    $ grunt



    # Test on local server
    $ grunt serve









