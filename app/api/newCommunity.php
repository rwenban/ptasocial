<?php

// good for a query string GET like $http.get('/api/newCommunity.php?name=russell').success(function (data) {
	// etc

// Prevent caching.
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 01 Jan 1996 00:00:00 GMT');

// The JSON standard MIME header.
header('Content-type: application/json');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

echo($request)

$name = $request;

// This NAME parameter is sent by our javascript client.
//$name = $_GET['name'];

// Here's some data that we want to send via JSON.
// We'll include the $id parameter so that we
// can show that it has been passed in correctly.
// You can send whatever data you like.
$data = array("name", $name);

// Send the data.
echo json_encode($data);

?>
