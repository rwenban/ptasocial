'use strict';

// create a module to make it easier to include in the app module

var app = angular.module('ptasocialApp', [
  'ui.bootstrap',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'rcWizard',
  'rcForm',
  'rcDisabledBootstrap'
]);

//
app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/login.html',
      controller: 'loginCtrl'
    })
    .when('/test', {
      templateUrl: 'views/sampleWizard.html'
    })
    .when('/form', {
      templateUrl: 'views/formwizard.html',
      controller: 'formwizardCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
  $locationProvider.html5Mode(true);
});