'use strict';

/*
  A 'dummy' service which loads a JSON file to simulate the dispathing of an email requesting email verificationion
 */

angular.module('ptasocialApp')
  .service('emailvalidation', function ($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var api = {
      async: function () {
        //since $http.get returns a promise,
        //and promise.then() also returns a promise
        //that resolves to whatever value is returned in it's
        //callback argument, we can return that.
        return $http.get('/model/test.json').then(function (result) {
          return result.data;
        });
      }
    };
    return api;

  });
