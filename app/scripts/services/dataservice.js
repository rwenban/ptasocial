'use strict';

angular.module('ptasocialApp')
  .service('dataservice', function () {
		var data = {
      username: '',
      email: ''
		};
    return {
			getData: function () {
				//You could also return specific attribute of the form data instead
				//of the entire data
				return data;
			},
			setData: function (newFormData) {
				//You could also set specific attribute of the form data instead
				data = newFormData;
			},
			resetData: function () {
				//To be called when the data stored needs to be discarded
				data = {};
			}
		};
  });
