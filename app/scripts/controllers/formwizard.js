'use strict';

// define controller for wizard

angular.module('ptasocialApp')
	.controller('formwizardCtrl', ['dataservice', '$scope', '$q', '$timeout', '$log', function (dataservice, $scope, $q, $timeout, $log) {

		$scope.saveState = function () {
			var deferred = $q.defer();

			$timeout(function () {
				deferred.resolve();
			}, 5000);

			return deferred.promise;
		};

		$scope.completeWizard = function () {
			$log.info('Completed!');
		};

		// values which populated by the user form
		$scope.user = {};

		// values which populated by the community form
		$scope.community = {};

		// balues which populated by the information/survey
		$scope.info = {};
		//
		// we could create a service here...
		$scope.createData = function () {

      $log.info('create data!');

			// the choice of roles
			$scope.community.roles = ['School Leadership',
				'PTA Chairperson',
				'PTA Committee',
				'School Teacher',
				'School Parent'];

			// need to set the default role
			$scope.community.role = $scope.community.roles[0];

			// the choice of countries
			$scope.community.countries = ['UK', 'USA', 'Other'];

			// set the default country
			$scope.community.country = $scope.community.countries[0];

			// for the number of pupils
			$scope.community.pupilnumbers = ['1 - 200', '201 - 400', '401 - 600', '601 - 800', '801 - 1000', '1001 - 1500', '1501 - 2000'];

			// set the default number of pupils
			$scope.community.pupilnumber = $scope.community.pupilnumbers[2];

			// for the information/survey
			$scope.info = {};

			// about
			$scope.info.informationSources = ['Other','Word of mouth','Twitter','Facebook','Email','Search engine', 'Blogs', 'Press', 'Pinterest'];

			// set the default value
			$scope.info.informationSource = $scope.info.informationSources[0];

			// for the blog radio buttons
			//
			$scope.info.choices = ['Email me when the blog is updated','No thanks'];

      $scope.info.blogChoice = $scope.info.choices[0];

      $log.info('choices: ', $scope.info.choices);

      $scope.info.blogChoice = $scope.info.choices[0];

      $log.info('choices: ', $scope.info.choices);
		};

		$scope.reset = function () {

		};

    $scope.addServiceData = function () {


      // add data from the dataservice
      $scope.data = dataservice.getData();

      $log.info('create service data!');

      $scope.user.userName = $scope.data.username;

      $scope.user.email = $scope.data.email;
    };

		//$scope.reset();

		$scope.createData();

    $scope.addServiceData();


		// on completion this method is called?
    //  TODO - check...
		$scope.completeWizard = function () {
			$log.info('The form has been completed!');
		};

	}]);