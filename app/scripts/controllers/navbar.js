'use strict';

angular.module('ptasocialApp')
  .controller('NavbarCtrl', function ($scope, $location) {

    $scope.menu = [
      {
        'title': 'Sign In',
        'link': '/'
      }
//      ,
//      {
//        'title': 'Form Test',
//        'link': '/test'
//      },
//      {
//        'title': 'Form Wizard',
//        'link': '/form'
//      }
    ];

    $scope.isActive = function (route) {
      return route === $location.path();
    };
  });