'use strict';

angular.module('ptasocialApp')
  .controller('CompletedformCtrl', [ '$scope', '$log', '$http', '$location', 'dataservice', function ($scope, $log, $http, $location, dataservice) {
		$scope.obj = dataservice.getData();

		$log.info('CompletedformCtrl || obj: ', $scope.obj);
  }]);
