'use strict';

/*
 The controller for the initial user credentials
 */

angular.module('ptasocialApp')
  .controller('loginCtrl', ['emailvalidation', 'dataservice', '$scope', '$location', '$timeout', function (emailvalidation, dataservice, $scope, $location, $timeout) {

    $scope.val = {};

    $scope.session = {};

    // when true the login credentials 'well' will hide itself
    $scope.isCollapsed = false;

    // the function perfomned on the 'form' submit
    $scope.login = function () {
      // calls the 'data service' to simulate a real server action
      // currently we are just keeping the values within this service Singleton

      // we have the session.username and session.email properties
      var data = {
        username: $scope.session.username,
        email: $scope.session.email
      };

      dataservice.setData(data);

      $scope.data = dataservice.getData();

      $scope.isCollapsed = true;

      // quick hack - direct link to the form...
      $timeout(function () {

        var url = '/form';

        $location.path(url);

      }, 3000);


    };
  }]);
