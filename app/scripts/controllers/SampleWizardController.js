'use strict';

angular.module('ptasocialApp')
  .controller('SampleWizardCtrl', [ '$scope', '$q', '$timeout', '$log', function ($scope, $q, $timeout, $log) {
    $log.info('!!hi from the SampleWizardCtrl');

    $scope.val = {};

    $scope.saveState = function () {

      console.log('save state');

      var deferred = $q.defer();

      $timeout(function () {
        deferred.resolve();
      }, 5000);

      return deferred.promise;
    };

    $scope.completeWizard = function () {
      $log.info('Completed!');
    };

  }]);
