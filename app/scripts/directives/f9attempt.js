'use strict';
/*
 Used for form submission to keep a reference to any previous attempt to submit.
 */
angular.module('ptasocialApp')
  .directive('f9Attempt', function () {
    return {
      restrict: 'A',
      // removed $scope to get round jshint error
      controller: ['$scope', function () {
        this.attempted = false;

        this.setAttempted = function () {
          console.log('f9Attempt::setAttempted');
          this.attempted = true;
        };
      }],
      // cElement, cAttributes, transclude
      compile: function () {
        return {
          pre: function (scope, formElement, attributes, attemptController) {
            scope.f9 = scope.f9 || {};
            scope.f9[attributes.name] = attemptController;
            console.log('f9Attempt::pre ', scope.f9[attributes.name]);
          },
          post: function (scope, formElement, attributes, attemptController) {
            formElement.bind('submit', function () {
              attemptController.setAttempted();
              if (!scope.$$phase) {
                scope.$apply();
              }
            });
          }
        };
      }
    };
  });
