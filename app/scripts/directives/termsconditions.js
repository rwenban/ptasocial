'use strict';

angular.module('ptasocialApp')
  .directive('termsConditions', function () {
    return {
      templateUrl: '/views/termsandconditions.html',
      restrict: 'AE'
    };
  });
