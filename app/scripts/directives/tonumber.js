'use strict';

/*
	Credit to Schmuli Raskin && Sander Elias
	See: https://groups.google.com/forum/#!msg/angular/Ecjx2fo8Qvk/x6iNlZrO_mwJ
	See: http://jsfiddle.net/ABE8U/
 */

angular.module('ptasocialApp')
	.directive('toNumber', function () {
		return {
			require: 'ngModel',
			link: function (scope, elem, attrs, ctrl) {
				ctrl.$parsers.push(function (value) {
					return parseFloat(value || '');
				});
			}
		};
	});
