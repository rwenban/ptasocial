'use strict';

angular.module('ptasocialApp')
  .directive('asteriskInfo', function () {
    return {
      templateUrl: '/views/asteriskinfo.html',
      restrict: 'E'
    };
  });
