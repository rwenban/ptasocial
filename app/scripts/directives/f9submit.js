'use strict';

/*
  Used for form validation
 */

angular.module('ptasocialApp')
  .directive('f9Submit', [ '$parse', function ($parse) {
    return {
      restrict: 'A',
      require: 'form',
      link: function (scope, formElement, attributes, formController) {

        var fn = $parse(attributes.f9Submit);

        formElement.bind('submit', function (event) {
          console.log('f9Submit::submit || valid form: ', formController.$valid );
          // if form is not valid cancel it.
          if (!formController.$valid) {

            //console.log('f9Submit::submit');
            return false;
          }

          scope.$apply(function () {
            fn(scope, {$event: event});
          });
        });
      }
    };
  }]);

